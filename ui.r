shinyUI(pageWithSidebar(
  wellPanel(
    h4("R with Shiny for Financial applications"),
    p("Computing volatility and using time series for data visualization" )
    ),
  
  sidebarPanel(
    
    wellPanel(
      p(strong("CAPM calculator")),
      selectInput("selected_security", "Select company:", 
                  choices = c("Citigroup Inc.","Emerson Electric", "Ford Motor","Pfizer Inc.","The Coca Cola Company")),
      #selectInput("rfr", "Select risk free rate:", 
                  #choices = c("0.10", "0.02", "0.03", "0.04", "0.05", "0.06", "0.07", "0.08","0.09")),
      #p('Reference: 91-day T-bill auction avg disc rate is 0.04 on 02-04-2014'),
      #selectInput("mrp", "Enter market risk premium:", 
                  #choices = c("0.025","0.010","0.015","0.020", "0.030", "0.035", "0.040", "0.045", "0.050", "0.055")),
      br(),
      sliderInput(inputId = "beta_time_num",
                  label = "Period length (in years)",
                  min = 1, max = 10, step = 1, value = 2),
      br(), 
      wellPanel(
        textOutput(outputId = "current_symbol_beta")
        #textOutput(outputId = "current_symbol_modeled_price"),
        #textOutput(outputId = "current_symbol_actual_price")
      ),
      #actionButton("recalcButton", "Compute Beta"),
      #br(),br(),
      wellPanel(
        textOutput(outputId = "current_symbol_info_revenue_multiple"),
        textOutput(outputId = "current_symbol_info_earnings_multiple"),
        textOutput(outputId = "current_symbol_info_earnings_multiple_forward"),
        textOutput(outputId = "current_symbol_info_earnings_multiple_pe_growth"),
        textOutput(outputId = "current_symbol_info_earnings_multiple_div_yield"),
        textOutput(outputId = "current_symbol_info_earnings_multiple_market_cap")
      ),
      
      #actionButton("dividendYield", "Show company info (multiples)"),
      #br(),br(),
      #checkboxInput(inputId = "some_input", label = "Show undervalued securities (CAPM):",     value = FALSE),
      #checkboxInput(inputId = "some_input", label = "Show overvalued securities (CAPM):",     value = FALSE),
      selectInput(inputId = "chart_type",
                  label = "Change chart type",
                  choices = c("Line" = "line",
                              "Candlestick" = "candlesticks",
                              "Matchstick" = "matchsticks",
                              "Bar" = "bars")
      )
    )
    
   # wellPanel(
      #p(strong("Charts")),
      #To do - provide option to directly overlay 
      #checkboxInput(inputId = "stock_inx", label = "SPDR S&P 500 ETF Trust (NYSEARCA:SPY)", value = FALSE),
      #p('An exchange traded fund. The Trust corresponds to the price and yield performance of the S&P 500 Index, and can be used as a performance benchmark.'),
      #checkboxInput(inputId = "stock_c", label = "Citigroup Inc. (C)",     value = TRUE),
      #checkboxInput(inputId = "stock_emr", label = "Emerson Electric (EMR)", value = FALSE),
      #checkboxInput(inputId = "stock_f", label = "Ford Motor", value = FALSE),
      #checkboxInput(inputId = "stock_pfe",  label = "Pfizer Inc. (PFE)",        value = FALSE),
      #checkboxInput(inputId = "stock_ko", label = "The Coca Cola Company (KO)",    value = FALSE)
    #),
    
    
    #wellPanel(
     # p(strong("Date range (back from present)")),
      #sliderInput(inputId = "time_num",
       #           label = "Time number",
        #          min = 1, max = 24, step = 1, value = 6),
      #
      #selectInput(inputId = "time_unit",
       #           label = "Time unit",
       #           choices = c("Days" = "days",
        #                      "Weeks" = "weeks",
         #                     "Months" = "months",
          #                    "Years" = "years"),
           #       selected = "Months")
    #),
    
    
  # checkboxInput(inputId = "log_y", label = "log y axis", value = FALSE)
  ),
  
  mainPanel(
    
    #conditionalPanel(condition = "input.stock_inx",
                     #br(),
                     #plotOutput(outputId = "plot_inx")),
    
    conditionalPanel(condition = "input.selected_security == 'Citigroup Inc.'",
                     br(),
                     div(plotOutput(outputId = "plot_c"))),               
    
    conditionalPanel(condition = "input.selected_security == 'Emerson Electric'",
                     br(),
                     div(plotOutput(outputId = "plot_emr"))),
    
    conditionalPanel(condition = "input.selected_security == 'Ford Motor'",
                     br(),
                     div(plotOutput(outputId = "plot_f"))),
    
    conditionalPanel(condition = "input.selected_security == 'Pfizer Inc.'",
                     br(),
                     div(plotOutput(outputId = "plot_pfe"))),
    
    conditionalPanel(condition = "input.selected_security == 'The Coca Cola Company'",
                     br(),
                     div(plotOutput(outputId = "plot_ko"))),
    br(),br(),
    wellPanel(
    br(),br(),
    p("Resources:"),
    p("The R Project for Statistical Computing - http://www.r-project.org" ),
    p("RStudio - http://www.rstudio.com/shiny/"),
    p("Quandl - http://www.quandl.com")
    )
  )
))