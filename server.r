#Project guide:
# Release 1:
#Use the data to compute the beta of the securities; recompute for the period

#- quandl has a lot of missing data - using multiples does not allow to model the price as well.
# ...... missing data ......
# k = rfr + b(Exp.rm - rfr)
# g = (1 - div.) x ROE
# p_now = d / (k - g)
# ... exlude the price modeling..
# ... IS and BS crash for some companies - shoudl probably exclude..
# !!! Add some info for beta - definition. 
# !!! Loading animation indicators - while computing and at the start - somehing that indicates activity?
# Change the iFrame in Joomla - from the cPanel - find the PHP layout;

#Later:

#Interpolate banchmark on top - chartSeries objects.. (addTa, ?)
#chart series is probably only for chart - maybe ready functions or some of the ready
# packages -fAsset, PerformanceAnalytics;
#2 allow the composition of portfolio - compute metrics and optmize - fPortfolio; ...
#3 allow the user to enter new symbols - i.e. compose portfolio or analize single security.
#4 allow to use derviatives ** ;
#5 analysis based on fundamentals.
#6 see packages for possible extensions.


if (!require(PerformanceAnalytics)) {
  stop("This app requires the PerformanceAnalytics package. To install it, run 'install.packages(\"PerformanceAnalytics\")'.\n")
}

if (!require(quantmod)) {
  stop("This app requires the quantmod package. To install it, run 'install.packages(\"quantmod\")'.\n")
}

if(!require(stringr)){
  stop("This package requires stringr package")
}

if(!require("plyr")){
  stop("This app requires plyr")
}

# Download data for a stock if needed, and return the data
require_symbol <- function(symbol, envir = parent.frame()) {
  if (is.null(envir[[symbol]])) {
    envir[[symbol]] <- getSymbols(symbol, auto.assign = FALSE)
  }
  
  envir[[symbol]]
}


shinyServer(function(input, output) {
  
  # Create an environment for storing data
  symbol_env <- new.env()
  
  # Make a chart for a symbol, with the settings from the inputs
  make_chart <- function(symbol) {
    symbol_data <- require_symbol(symbol, symbol_env)
    
    chartSeries(symbol_data,
                name      = symbol,
                type      = input$chart_type,
                subset    = paste("last", 12, "months"),
                log.scale = FALSE,
                theme     = "white")
    
  }
  
  #
  output$current_symbol_beta <- renderText("Beta computation for Citigroup Inc.: n/a") 
  output$current_symbol_modeled_price <- renderText("Modeled price: n/a") 
  output$current_symbol_actual_price <- renderText("Actual price: n/a") 
  output$current_symbol_info_revenue_multiple <- renderText("Info: n/a") 
  output$current_symbol_info_earnings_multiple <- renderText("")
  output$current_symbol_info_earnings_multiple_forward  <- renderText("")
  output$current_symbol_info_earnings_multiple_pe_growth  <- renderText("")
  output$current_symbol_info_earnings_multiple_div_yield  <- renderText("")
  output$current_symbol_info_earnings_multiple_market_cap  <- renderText("")
  
  recompute <- function()
  {
    symbol <- input$selected_security
    RFR <- input$rfr
    mrp <- input$mrp
    # Defining the current and starting date based on the user choice.
    period_length <- input$beta_time_num
    period_start_year <- as.character((2014 - period_length))
    period_beta_end_date <- Sys.Date()
    d <- as.POSIXlt(Sys.Date())
    d$year <- d$year-period_length
    period_beta_begin_date <- as.Date(d)
    #
    beta_computation <- compute_beta(symbol,RFR, mrp, period_beta_begin_date,period_beta_end_date)
    #
    output_full <- paste("Beta computation for", symbol, ":")
    beta_to_string <- as.character(beta_computation)
    beta_to_string_short <- substr(beta_to_string, start=0, stop=6) 
    output_with_beta <- paste(output_full, beta_to_string_short)
    output$current_symbol_beta <- renderText(output_with_beta)
    print(output_with_beta)
    print(period_beta_begin_date)
    print(symbol)
    print(RFR)
    print(mrp)
    print(beta_computation)
    #
    symbol <- input$selected_security
    dividend_yield(symbol)
    #dy <- dividend_yield(symbol)
    #print(dy)
    print(symbol)
  }
  compute_beta <- function(symbol, rfr, mrp, start_date, end_date){
    #This example demostrates how to compute beta - Chapter 3.
    # Estimating beta of Google, mkt - SP500, rfr - LIBOR;
    
    library(Quandl)
    #setting the session token
    Quandl.auth("GsZGaqViWGcSPQ22Jp3o")
    
    symbol_code <- "GOOG/NYSE_C"
    if(symbol == "Citigroup Inc.")
    {
      symbol_code <- "GOOG/NYSE_C"
    }else if(symbol == "Emerson Electric")
    {
      symbol_code <- "GOOG/NYSE_EMR"
    }else if(symbol == "Ford Motor")
    {
      symbol_code = "GOOG/NYSE_F"
    }else if(symbol == "Pfizer Inc.")
    {
      symbol_code = "GOOG/NYSE_PFE"
    }else if(symbol == "The Coca Cola Company")
    {
      symbol_code = "GOOG/NYSE_KO"
    }else{
      return ("N/A")
    }
    
    G <- Quandl(symbol_code, start_date = start_date, end_date = end_date)
    #str(G)
    
    #Using only the closing values
    G <- G$Close
    
    #The same with the market data - i.e. S&P 500 + Note - using the adjusted close values:
    
    SP500 <- Quandl("YAHOO/INDEX_GSPC", start_date = start_date, end_date = end_date)
    # str(SP500)
    
    #Using only the closing values - meaning corrected for dividends and splits;
    SP500 <- SP500$'Adjusted Close'
    
    #3d we need the RFR (Risk free rate) time series - we will use the 1 month USD LIBOR
    # assuming the 1 month rate is less affected by noise:
    
    LIBOR <- Quandl("FED/RILSPDEPM01_N_B", start_date = start_date, end_date = end_date)
    #str(LIBOR)
    
    LIBOR <- LIBOR$Value
    
    #NOTE: have vectors (G, SP500, LIBOR) of different lenght and we used Closing, Adjusted and simlpy the Value options
    
    sapply(list(G, SP500, LIBOR), length)
    
    G     <- Quandl(symbol_code, start_date = start_date, end_date = end_date)
    SP500 <- Quandl('YAHOO/INDEX_GSPC', start_date = start_date, end_date = end_date)
    LIBOR <- Quandl('FED/RILSPDEPM01_N_B', start_date = start_date, end_date = end_date)
    
    #We call the Reduce function to identify the common dates in the three time series:
    
    cdates <- Reduce(intersect, list(G$Date, SP500$Date,LIBOR$Date))
    
    #Now, let us simply filter all three data frames to the relevant cells to get the vectors:
    G      <- G[G$Date %in% cdates, 'Close']
    SP500  <- SP500[SP500$Date %in% cdates, 'Adjusted Close']
    LIBOR  <- LIBOR[LIBOR$Date %in% cdates, 'Value']
    
    #After downloading and cleaning the data, you have to calculate the log-returns (rt) 
    #for stock and the market index
    
    logreturn <- function(x) log(tail(x, -1) / head(x,-1))
    
    rft <- log(1 + head(LIBOR, -1)/36000 * diff(cdates))
    #str(rft)
    
    #Once we have both time series; the individual asset's (Google, in our case) and the market's (S&P 500) 
    #risk premium, beta can be calculated based on equation
    
    cov(logreturn(G) - rft, logreturn(SP500) - rft) / var(logreturn(SP500) - rft)
    
    #This could be also simplified by adding a new function to describe the risk premium:
    riskpremium <- function(x) logreturn(x) - rft
    beta <- cov(riskpremium(G), riskpremium(SP500)) / var(riskpremium(SP500))
    
    #Obtain from good resource and mathc to the investment horizon
    marketriskpremium <- .025
    #RFR is coming from the user input:
    riskfreerate <- as.numeric(rfr)
    
    print('beta:')
    print(beta) 
    
    expReturn <- (riskfreerate + beta * (riskfreerate - marketriskpremium))
    
    print('Expected return on the stock based on E(mr):')
    print(expReturn)
    
    #test
    #2013 data entries are missing - last entry for 2013
    #        Date Return on Equity
     # 1 2012-12-31           0.0616
    
    # !!! beta is realistic with 10 years of data:
    
    #modeled_stock_price("C", beta, 0.065, 0.02, "2012-01-01","2014-01-01")
    
    return (beta) # The main task of the function
    
    #Estimating the expected return on the stock - using RFR, E(rm), and beta?
    
    #The result matches the one in the book - [1] 0.8999759
    #See the book for beta estimation using linear regression.
    
    #You can also find built-in functions in the PerformanceAnalytics package, 
    #CAPM. alpha and CAPM.beta, that calculate the parameters alpha and beta for a given asset. 
    #The requested parameters are the series of the asset's and the benchmark asset's return and the 
    #risk-free rate.
  }
  
  compute_beta_user_variable <- function(symbol, start_date, end_date){
    return (symbol)
  }
  
  modeled_stock_price <-function(symbol, beta, rfr, mrp, start_date, end_date)
  {
    #!!! beta for at least 10 years 
    
    k_ce <- rfr + beta * mrp # where the market premium = E(mtk) - rfr
    print('cost of Eq')
    print(k_ce) #For Citigroup about 11% - quite realistic
    
    #gold20 <- Quandl("OFDP/FUTURE_GC2", collapse="monthly", 
                     #start_date="2013-06- 01", end_date="2014-01-01")
    #print('testing gold futures contract availability')
    #print(gold20)
    
    #getFinancials usage:
    #getFinancials(Symbol, env = parent.frame(), src = "google", auto.assign = TRUE,...)
    #viewFinancials(x, type=c('BS','IS','CF'), period=c('A','Q'),subset = NULL)
    # Use the IS and BS from the annual records - still for the end of 2013 but ...
    # for educational purposes..
    
    # If conditions to adjust the query:
    
    C <- getFinancials('F')
    #
    fundamentals_IS_Frame <- viewFinancials(C.f, "IS", "A")
    fundamentals_BS_Frame <- viewFinancials(C.f, "BS", "A")
    #
    #print(fundamentals_IS_Frame)
    #print(fundamentals_BS_Frame)
    
    net_income <- fundamentals_IS_Frame[25,1]
    retained_earnings <- fundamentals_BS_Frame[36,1]
    #print(net_income)
    #print(retained_earnings)
    #
    ROE_from_financials <- net_income / retained_earnings
    print('roe from financials')
    print(ROE_from_financials)
    # G
    # g = (1 - div. payout ratio) x ROE = (1 - DPS/EPS) x ROE = Plowback ratio X ROE.
    DPS <- fundamentals_IS_Frame[36,1]
    print('DPS:')
    print(DPS)
    
    EPS_Diluted_Normalized <- fundamentals_IS_Frame[49,1]
    print('EPS:')
    print(EPS_Diluted_Normalized)
    
    plowbackRatio <- 1 - (DPS / EPS_Diluted_Normalized)
    print('plowback ratio')
    print(plowbackRatio)
    
    g <- plowbackRatio * ROE_from_financials
    print('constant grwoth rate from fundamentals')
    print(g)
    # 
    #http://www.quandl.com/api/v1/datasets/OFDP/DMDRN_C_ROE.csv
    #ROE <- Quandl("OFDP/DMDRN_C_ROE", collapse="monthly", 
                 # start_date="2012-06- 01", end_date="2013-01-01")
    #print(ROE)
    
    #Below annual dividend amount from Citigroup for 2013:
    
    dividend <- 0.0100 * 4 #Quandl("OFDP/DMDRN_C_ROE", collapse="monthly", 
                  # start_date="2012-06- 01", end_date="2013-01-01")
    
    if(k_ce < g)
      print('!!! cannot use the Gordon model - return on Eq less that g')
    
    intrinsic_value = dividend / (k_ce - g)
    
    print('intrinsic value:')
    print(intrinsic_value)
  }
  
  dividend_yield <- function(symbol)
  {
    #######################################################################
    # Script to download key metrics for a set of stock tickers using the quantmod package
    #######################################################################
    require(quantmod)
    require("plyr")
    what_metrics <- yahooQF(c("Price/Sales", 
                              "P/E Ratio",
                              "Price/EPS Estimate Next Year",
                              "PEG Ratio",
                              "Dividend Yield", 
                              "Market Capitalization",
                              "Dividend Per Share",
                              "Return on Equity"))
    
    symbol_code <- "C"
    if(symbol == "Citigroup Inc.")
    {
      symbol_code <- "C"
    }else if(symbol == "Emerson Electric")
    {
      symbol_code <- "EMR"
    }else if(symbol == "Ford Motor")
    {
      symbol_code = "F"
    }else if(symbol == "Pfizer Inc.")
    {
      symbol_code = "PFE"
    }else if(symbol == "The Coca Cola Company")
    {
      symbol_code = "KO"
    }else{
      return ("N/A")
    }
    
    tickers <- c(symbol_code)
    # Not all the metrics are returned by Yahoo.
    metrics <- getQuote(paste(tickers, sep="", collapse=";"), what=what_metrics)
    
    #Add tickers as the first column and remove the first column which had date stamps
    metrics <- data.frame(Symbol=tickers, metrics[,2:length(metrics)]) 
    revenue_multiple <- metrics[1,2]
    earnings_multiple <- metrics[1,3]
    earnings_multiple_forward  <- metrics[1,4]
    pe_growth  <- metrics[1,5]
    div_yield  <- metrics[1,6]
    market_cap  <- metrics[1,7]
    dividend  <- metrics[1,8]
    roe  <- metrics[1,9]
    print(revenue_multiple)
    print(dividend)
    print(roe)
    #Change colnames
    colnames(metrics) <- c("Symbol", "Revenue Multiple", "Earnings Multiple", 
                           "Earnings Multiple (Forward)", "Price-to-Earnings-Growth", "Div Yield", "Market Cap")
    
    print(metrics)
    #1 2.13 10.91 8.38 0.49 0.08 149.5B
    #Readable output:
    metrics_text1 <- paste("Revenue Multiple:",as.character(revenue_multiple) )
    metrics_text2 <- paste("Earnings Multiple:",as.character(earnings_multiple))
    metrics_text3 <- paste("Earnings Multiple (Forward):",as.character(earnings_multiple_forward))
    metrics_text4 <- paste("Price-to-Earnings-Growth:",as.character(pe_growth))
    metrics_text5 <- paste("Dividend Yield:",as.character(div_yield))
    metrics_text6 <- paste("Market Cap:",as.character(market_cap))
    output$current_symbol_info_revenue_multiple <- renderText(metrics_text1)
    output$current_symbol_info_earnings_multiple <- renderText(metrics_text2)
    output$current_symbol_info_earnings_multiple_forward <- renderText(metrics_text3)
    output$current_symbol_info_earnings_multiple_pe_growth <- renderText(metrics_text4)
    output$current_symbol_info_earnings_multiple_div_yield <- renderText(metrics_text5)
    output$current_symbol_info_earnings_multiple_market_cap <- renderText(metrics_text6)
    return(metrics$DividendYield)
    #Persist this to the csv file
    #write.csv(metrics, "FinancialMetrics.csv", row.names=FALSE)
    
    #######################################################################
  }
  
  
  
  #observe({
   # if(input$dividendYield == 0)
   # {
      #return('nothing to be done - register observe')
   # }
    
    #isolate({
      # Your arbitrary R code goes here
      #symbol <- input$selected_security
      #dividend_yield(symbol)
      #dy <- dividend_yield(symbol)
      #print(dy)
      #print(symbol)
   # })
  #})
  
  #
  #observe({
    # Don't do anything if the button has never been pressed
  #if (input$recalcButton == 0)
  #{
  #  return('nothing to be done - register observe')
  #}
      
  #isolate({
      # Your arbitrary R code goes here
  #symbol <- input$selected_security
  #RFR <- input$rfr
  # mrp <- input$mrp
      # Defining the current and starting date based on the user choice.
  # period_length <- input$beta_time_num
  #period_start_year <- as.character((2014 - period_length))
  #period_beta_end_date <- Sys.Date()
  #d <- as.POSIXlt(Sys.Date())
  #d$year <- d$year-period_length
  #period_beta_begin_date <- as.Date(d)
      #
  #beta_computation <- compute_beta(symbol,RFR, mrp, period_beta_begin_date,period_beta_end_date)
      #
  #output_full <- paste("Beta computation for", symbol, ":")
  #beta_to_string <- as.character(beta_computation)
  #beta_to_string_short <- substr(beta_to_string, start=0, stop=6) 
  #output_with_beta <- paste(output_full, beta_to_string_short)
  #output$current_symbol_beta <- renderText(output_with_beta)
  #print(output_with_beta)
  #print(period_beta_begin_date)
  #print(symbol)
  #print(RFR)
  #print(mrp)
  # print(beta_computation)
  # })
#})
  
  #observe({
   # if(input$dividendYield == 0)
    #{
      #return('nothing to be done - register observe')
    #}
    
    #isolate({
      # Your arbitrary R code goes here
      #symbol <- input$selected_security
      #dividend_yield(symbol)
      #dy <- dividend_yield(symbol)
      #print(dy)
      #print(symbol)
    #})
  #})
  
  observe({
    # Don't do anything if the button has never been pressed
    if (input$selected_security == 'Citigroup Inc.')
    {
      #print('Citigroup Inc.')
      #output$current_symbol_beta <- renderText('Computing beta..')
    }
    
    if (input$beta_time_num == 0)
    {
      
    }
    
    isolate({
      print('update values')
      recompute()
    })
  })
  
  
  output$plot_c <- renderPlot({ make_chart("C") })
  output$plot_emr <- renderPlot({ make_chart("EMR") })
  output$plot_pfe  <- renderPlot({ make_chart("PFE")  })
  output$plot_ko <- renderPlot({ make_chart("KO") })
  output$plot_f <- renderPlot({ make_chart("F") })
  output$plot_inx  <- renderPlot({ make_chart("SPY") })
  output$beta_computation <- renderText(compute_beta("NULL","2009-06-01","2013-06-01"))
  output$beta_computation_user_input <- renderText(compute_beta_user_variable(input$selected_security,"2009-06-01","2013-06-01"))
  
  
})


